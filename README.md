## repo:
hello app python

# Start flask server:
`python app.py`

# Build image:
`docker build -t dejanualex/hello_python:1 .`

# Start container:
`docker run -d -p 5555:5555 dejanualex/hell_python:1`
